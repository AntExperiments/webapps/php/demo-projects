<?php
    $text = "Guten Tag Herr Vollzugsbeamter";
    
    function textumwandlung($text, $arg) {
        if ($arg == "g") {
            echo strtoupper($text) . "<br>";
        } else if ($arg == "k") {
            echo strtolower($text) . "<br>";;
        } else {
            echo "Achtung! Falscher Modifikationsparameter! Gültig sind nur  \"g\" und \"k\"!<br>";
        }
    }

    textumwandlung($text, "g");
    textumwandlung($text, "k");
    textumwandlung($text, "x");
?>