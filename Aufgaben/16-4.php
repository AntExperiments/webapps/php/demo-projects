<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
        <link type="text/css" rel="stylesheet" href="css/materialize.min.css"  media="screen,projection"/>
        <title>16-4</title>
    </head>

    <body>
        <nav>
            <div class="nav-wrapper">
                <a href="." class="brand-logo center">16-4 - Input into database</a>
            </div>
        </nav>
        <body>

        <div style="height: 10px"></div>

        <div class="container">
            <div class="row">
                <form class="col s12" action="16-4.php" method="post">
                    <div class="row">
                        <div class="input-field col s6">
                        <input id="name" type="text" class="validate" name="name">
                        <label for="name">Name</label>
                        </div>
                        <div class="input-field col s6">
                        <input id="email" type="email" class="validate" name="email">
                        <label for="email">Email</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s12">
                        <input id="message" type="text" class="validate" name="message">
                        <label for="message">Message</label>
                        </div>
                    </div>
                    <div class="row">
                    <a class="waves-effect waves-light btn" onclick="document.getElementsByTagName('form')[0].submit()">Add to Database</a>
                    </div>
                </form>
            </div>
        </div>
    </body>

        <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
    </body>
</html>
<?php
    if ($_SERVER['REQUEST_METHOD'] == 'POST') {

        // Create connection
        $connection = mysqli_connect("127.0.0.1","phpmodul","Siemens_01","phpmodul");
        // Check connection
        if (!$connection) {
            die("Connection failed: " . mysqli_connect_error());
        }

        $sql = "INSERT INTO messageboard (name, email, message, datum) VALUES ('" . 
        $_POST['name'] . "', '" . $_POST['email'] . "', '" . $_POST['message'] . "', '" . date('Y-m-d') . "')";

        if (mysqli_query($connection, $sql)) {
            echo "<script>M.toast({html: 'Added Entry successfully!'})</script>";
        } else {
            echo "Error: " . $sql . "<br>" . mysqli_error($connection);
        }

        mysqli_close($connection);
    }
?>