<?php
    if (date('h') < 12) {
        echo "Good Morning<br>";
    } else if (date('h') < 17) {
        echo "Good Afternoon<br>";
    }else if (date('h') < 21) {
        echo "Good Evening<br>";
    } else {
        echo "Hello<br>";
    }

    echo "Today is " . strftime('%A') . ", the " . date('d') . "th of " . strftime('%B %Y') . ", it's " . date("h:i") . " o'clock"; 
?>