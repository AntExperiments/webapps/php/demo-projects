<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
        <link type="text/css" rel="stylesheet" href="css/materialize.min.css"  media="screen,projection"/>
        <title>16-3</title>
    </head>

    <body>
        <nav>
            <div class="nav-wrapper">
                <a href="16-1.php" class="brand-logo center">SQL is COLORFULLLLL</a>
            </div>
        </nav>
        <body>
        <div style="height: 10px"></div>

            <div class="container">

                <table class="striped">
                <?php
                    $mysqli = new mysqli("127.0.0.1","phpmodul","Siemens_01","phpmodul");

                    // Check connection
                    if ($mysqli -> connect_errno) {
                        echo "Failed to connect to MySQL: " . $mysqli -> connect_error;
                        exit();
                    }

                    if ($result = $mysqli -> query("SELECT * FROM messageboard")) {
                        /* fetch associative array */
                        while ($row = $result -> fetch_assoc()) {
                            echo "<tr style=\"background-color:";

                            if ($row['ID'] % 2 == 0) {
                                echo "#DDDDDD";
                            } else {
                                echo "#FFFFFF";
                            }
                            
                            echo "\"><th>" . $row['ID'] . "</th><th>" . $row['name'] . "</th><th>". $row['email'] . "</th><th> ". $row['message'] . "</th><th>" . $row['datum'] . "</th>";
                        }
                        $result->free();
                    }
                    $mysqli -> close();
                    ?>
                </table>
            </div>
        </body>

        <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
    </body>
</html>
