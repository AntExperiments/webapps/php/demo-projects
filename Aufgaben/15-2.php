<?php

if ( $_SERVER['REQUEST_METHOD'] == 'POST' ) {
    $fp = fopen('15-2.txt', 'a');
    fwrite($fp, $_POST['url'] . "\n");  
    fclose($fp);  
}

?>
<!DOCTYPE html>
<html>
    <head>
        <!--Import Google Icon Font-->
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <!--Import materialize.css-->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">

        <!--Let browser know website is optimized for mobile-->
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    </head>
    <body>
        <nav>
            <div class="nav-wrapper">
                <a href="#" class="brand-logo center">Hyperlink manager</a>
            </div>
        </nav>

        <div style="height: 10px"></div>

        <div class="container">
            <form action="15-2.php" method="post">
            <div class="input-field col s6">
                <input placeholder="insert URL here" id="url" name="url" type="text" class="validate">
                <label for="url">Add Hyperlink</label>
            </div>
            </form>

            <ul class="collection">
            <?php
                $file = file("15-2.txt");
                foreach($file as $f){
                    echo "<li class=\"collection-item\"><a target=\"_blank\" href=\"" . $f . "\">" . $f . "</a></li><br>";
                }
            ?>
            </ul>
        </div>

        <!--JavaScript at end of body for optimized loading-->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
    </body>
</html>