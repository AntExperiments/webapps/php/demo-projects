## Include/Require

`include` und `require` sind eine möglichkeit text/code/markup/php/... in eine 

php-datei "einzufügen". Dies ist für viele zweche sehr hilfreich und ersparrt einiges an Arbeit (und verhindert redundanten code).

Beispiel:
```php
<html>
    <head>
        <?php require 'head.php';?>
    </head>
    <body>
        <h1>Welcome to my home page!</h1>
        <p>There will be a lot of stupidity here</p>
        <?php include 'footer.php';?>
    </body>
</html> 
```

Der Unterschied zwischen denen ist, dass `include` einfach eine Fehlermeldung ausgiebt fals etwas nicht funktioniert und `require` stoppt das komplette Script.


## include_once/require_once

Eigentlich ziemlich das selbe wie ohne "_once" aber wird nur einmal ausgeführt, giebt ein true/false aus

## Verwendungszwecke
require:        parrameter in seperater datei (z.B. config datei)
include:        navigationsleiste, **footer**, ... der auf jeder Seite gleich ist
include_once:   prüft ob sprache korrekt gesetzt
require_once:   Überprüffung ob eingeloggt