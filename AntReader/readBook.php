<?php
    $book = $_GET["book"];

    if ($file = fopen("_books/$book/content.md", "r")) {
        while(!feof($file)) {
            $line = fgets($file);
            # do same stuff with the $line
            if (strlen($line) != 1) {
                if ($line[0] == '#') {
                    if ($line[1] == '#') {
                        echo '<h2>' . substr($line, 3) . '</h2>';
                    } else {
                        echo '<h1>' . substr($line, 2) . '</h1>';
                    }
                } else {
                    echo "<p>$line</p>";
                }
            }
        }
        fclose($file);
    }

?>