<html>
    <?php
        $greetings = array("Hello", "Hey", "Hi", "Welcome", "Good Morning");
        $vorname = "Yanik";
        $nachname = "Ammann";

        function getGreeting() {
            global $greetings, $vorname, $nachname;

            echo $greetings[rand(0, sizeof($greetings) -1)];
            echo ", $vorname $nachname";
        }
    ?>
    <head>
        <title>demo4</title>
        <style>
            @import url('https://fonts.googleapis.com/css?family=Baloo+2&display=swap');
            * {
                font-family: 'Baloo 2', cursive;
            }
            html {
                background-image: url('https://stored.ant.lgbt/img/gallery/tree.jpg');
                background-size: 100%;
            }
            input {
                background-color: #ddd;
            }
        </style>
    </head>
    <body>
        <center>
            <h1 style="margin-top: 40vh; color: white; font-size: 46px">
                <?php getGreeting() ?>
            </h1>
            <form action="demo4_alt.php" method="post">
                <input type="text" size="17" name="Username">
                <input type="submit" value="OK">
            </form>
        </center>
    </body>
</html>

