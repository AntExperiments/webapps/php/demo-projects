<?php
    if ($_SERVER['REQUEST_METHOD'] === 'POST')
        file_put_contents('src/comments/' . $_GET['post'], $_POST['user'] . "$" . $_POST['url'] . "$" . $_POST['content'] . "\n", FILE_APPEND);
?>
<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css"><link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

        <title>My Awesome Blog</title>
    </head>

    <body>
        <nav style="padding-left: 15px" class="teal">
            <div class="nav-wrapper">
            <a href="." class="breadcrumb">Home</a>
                <?php
                    if (isset($_GET['post']))
                    if ($_GET['post'] == "list") {
                        echo "<a href='?post=list&page=1' class='breadcrumb' style='text-transform: capitalize'>Posts</a>";
                    } else {
                        if (!isset($_GET['isPage']))
                            echo "<a href='?post=list&page=1' class='breadcrumb' style='text-transform: capitalize'>Posts</a>";
                        echo "<a href='' class='breadcrumb' style='text-transform: capitalize'>" . $_GET['post'] . "</a>";
                    }
                ?>
            <ul id="nav-mobile" class="right hide-on-med-and-down">
                <?php
                    foreach(scandir("src/pages") as $dir)
                        if (!is_dir($dir))
                            echo "<li><a href='?post=$dir&isPage' style='text-transform: capitalize'>$dir</a></li>";
                ?>
                <li><a href="?post=list&page=1">Posts</a></li>
                <li><a href="admin">Admin</a></li>
            </ul>
            </div>
        </nav>


        <div style="height: 10px"></div>

        <div class="container">
            <?php
            if (isset($_GET["post"])) {
                // post
                if ($_GET["post"] == "list") {
                    // list blog posts
                    echo "<ul class='collection with-header' style='border:none'>";
                    echo "<li class='collection-header'><h4>Blog Posts</h4></li>";

                    $files = scandir('src/posts');
                    $offset = 0;
                    if (isset($_GET['page']))
                        $offset = ($_GET['page'] - 1) * 10;
                    for ($i=2 + $offset; $i < 10 + 1 + $offset; $i++)
                        if (isset($files[$i]))
                            echo "<li class=\"collection-item\"><a style='color:teal' href=\"?post=$files[$i]\">$files[$i]<span style='float:right'>" . date ("Y-m-d H:i", filemtime('src/posts/' . $files[$i])) ."</span></a></li>";
                    echo "</ul><br>";
                    if ($_GET['page'] == 1) {
                        echo "<ul class='pagination'><li class='disabled'><a><i class='material-icons'>chevron_left</i></a></li>";
                    }
                    else {
                        echo "<ul class='pagination'><li><a href='?post=list&page=" . ($_GET['page'] - 1) . "'><i class='material-icons'>chevron_left</i></a></li>";
                    }
                    
                    for ($j=1; $j < (sizeof($files) % 10) - 1 ; $j++)
                        if ($j == $_GET['page'])
                            echo "<li class='active teal'><a href='?post=list&page=$j'>$j</a></li>";
                        else
                            echo "<li class='waves-effect'><a href='?post=list&page=$j'>$j</a></li>";

                    if ($_GET['page'] == (sizeof($files) % 10) - 1)
                        echo "<li class='disabled'><a><i class='material-icons'>chevron_right</i></a></li></ul>";
                    else
                        echo "<li class='waves-effect'><a href='?post=list&page=" . ($_GET['page'] + 1) . "'><i class='material-icons'>chevron_right</i></a></li></ul>";
                } else {
                    // get post content
                    if (file_exists('src/pages/' . $_GET['post']))
                        $filename = 'src/pages/' . $_GET['post'];
                    else if (file_exists('src/posts/' . $_GET['post']))
                        $filename = 'src/posts/' . $_GET['post'];
                    else
                        echo "<h1>404 Post or Page not found</h1>";

                    // get post comments
                    $handle = fopen($filename, "r");
                    if ($handle) {
                        $i = 0;
                        echo "<h1 style='text-transform: capitalize'>" . str_replace("-", " ", $_GET['post']) . "</h1>";
                        while (($line = fgets($handle)) !== false)
                            echo "<p>$line</p>";
                        fclose($handle);
                    if (!isset($_GET['isPage'])) {
                        echo "<br><div class='col s12 m8 offset-m2 l6 offset-l3'>
                            <div class='card-panel grey lighten-5 z-depth-1'>";
                        if (!file_exists("src/comments/" . $_GET['post'])) {
                            echo "
                                    <div class='row valign-wrapper'>
                                        <div class='col s12'>
                                            <span class='black-text'>No comments available yet, be the first one to write one</span>
                                        </div>
                                    </div><hr><br>
                                ";
                        } else {
                            $handleCommend = fopen("src/comments/" . $_GET['post'], "r");
                            if ($handleCommend) {
                                while (($line = fgets($handleCommend)) != false)
                                    echo    "<div class='row valign-wrapper'>
                                            <div class='col s2'>
                                                <img src='". explode("$", $line)[1] ."' alt='". explode("$", $line)[0] ."' class='circle responsive-img' style='object-fit: cover;height: 64px;width: 64px'>
                                            </div>
                                                <div class='col s10'>
                                            <span class='black-text'>". explode("$", $line)[2] ."</span>
                                            </div></div>";
                            fclose($handleCommend);
                        } echo "<hr><br>";
                        }

                        echo "
                            <div class='row'>
                            <form class='col s12' action='' method='post'>
                                <div class='row' style='margin-bottom: 0'>
                                    <div class='input-field col s8'>
                                    <input name='user' placeholder='John Doe' id='user' type='text' class='validate'>
                                    <label for='user'>Username</label>
                                    </div>
                                    <div class='input-field col s4'>
                                    <input placeholder='https://stored.ant.lgbt/img/icon_225.png' name='url' id='url' type='url' class='validate'>
                                    <label for='url'>Url of Image</label>
                                    </div>
                                </div>
                                <div class='row' style='margin-bottom: 0'>
                                    <div class='input-field col s12'>
                                    <input name='content' id='content' type='text'>
                                    <label for='content'>Message</label>
                                    </div>
                                </div>
                                <button class='btn waves-effect waves-light' type='submit' name='action'>Submit
                                    <i class='material-icons right'>send</i>
                                </button>
                            </form>
                        </div>";
                    }

                    echo"</div></div>";
                    }
                }
            } else{  
                // home
                echo "
                    <h1>Welcome to my awesome Blog</h1>
                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Vel corrupti praesentium ipsa maiores itaque. Error id illum dolorem voluptate et commodi asperiores explicabo! Blanditiis similique molestiae asperiores quisquam laudantium pariatur.</p>
                ";
            }
            ?>
        </div>

        <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
    </body>
</html>